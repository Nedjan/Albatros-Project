/**
 * Created by Marjo on 11/1/2015.
 */
app.controller('DashboardCtrl', ['$scope', 'DataFactory', 'ngDialog', function($scope, DataFactory, ngDialog){
    console.log('[DashboardCtrl Startde ...]');
    $scope.actractions = DataFactory.actractions;

    $scope.showDialog = function() {
        ngDialog.open({
            template: 'templates/dashboard/dashboard.html',
            scope: $scope
        })
    }
}]);